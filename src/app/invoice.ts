export class Invoice {
  id: number;
  paymentDueDate: string;
  clientName: string;
  jobDesc: string;
  total: number;
}