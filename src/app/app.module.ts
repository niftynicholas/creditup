import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RetrieveService } from './shared/retrieve.service';
import { DataService } from './shared/data.service';
import { PaymentsComponent } from './payments/payments.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { HeaderComponent } from './header/header.component';
import { ContactUsComponent } from './contactus/contactus.component';
import { AccountComponent } from './account/account.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateInvoiceComponent } from './createinvoice/createinvoice.component';
import { Globals } from './globals';
import { FinancialPlanningComponent } from './financialplanning/financialplanning.component';
import { ReportGenerationComponent } from './reportgeneration/reportgeneration.component';
import { CreditJourneyComponent } from './creditjourney/creditjourney.component';

// Define the routes
const ROUTES = [
  {
    path: '',
    redirectTo: 'retrieve',
    pathMatch: 'full'
  },
  { path: 'retrieve', component: HomeComponent },
  { path: 'payments', component: PaymentsComponent },
  { path: 'contactus', component: ContactUsComponent },
  { path: 'account', component: AccountComponent },
  { path: 'invoice', component: InvoiceComponent },
  { path: 'createinvoice', component: CreateInvoiceComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'financialPlanning', component: FinancialPlanningComponent },
  { path: 'creditJourney', component: CreditJourneyComponent },
  { path: 'reportGeneration', component: ReportGenerationComponent }
,];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    PaymentsComponent,
    DropdownDirective,
    ContactUsComponent,
    AccountComponent,
    InvoiceComponent,
    CreateInvoiceComponent,
    DashboardComponent,
    FinancialPlanningComponent,
    ReportGenerationComponent, 
    CreditJourneyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES) // Add routes to the app
  ],
  providers: [RetrieveService, DataService, Globals],  // Add the Retrieve and Data service
  bootstrap: [AppComponent]
})
export class AppModule { }
