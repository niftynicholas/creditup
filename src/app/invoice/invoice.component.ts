import { Component, OnInit} from '@angular/core';
import { RetrieveService } from '../shared/retrieve.service';
import { DataService } from '../shared/data.service';
import { Invoice } from '../invoice';
import { Globals } from '../globals';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  invoices: Invoice[];
  selectedInvoice: Invoice;
  paymentDueDate: string;
  clientName: string;
  jobDesc: string;
  total: number;

  constructor(private globals: Globals) { 
    this.invoices = globals.invoices;
    console.log(this.globals.invoices);
  }

  ngOnInit() {
  }

  onClick() {
    this.invoices.push({id: this.invoices.length + 1, paymentDueDate: this.paymentDueDate, clientName: this.clientName, jobDesc: this.jobDesc, total: this.total});
    this.globals.invoices = this.invoices;
    this.paymentDueDate = '';
    this.clientName = '';
    this.jobDesc = '';
    this.total = 0.00;
  }

  onSelect(invoice: Invoice): void {
    this.selectedInvoice = invoice;
    console.log(invoice + " selected!");
  }
}